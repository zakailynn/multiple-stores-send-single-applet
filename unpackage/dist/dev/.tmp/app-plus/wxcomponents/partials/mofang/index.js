const app = getApp()
Component({
  properties: {
    data: {
      type: JSON,
    },
  },
  data: {
  },
  ready:function(){
    console.log('=====ready-MoFang====', this.data.data)
    this.setData({ items: this.data.data.jsonData.items || []})
    this.setData({ width: Number(this.data.data.jsonData.width)||0})
    this.setData({ height: Number(this.data.data.jsonData.height) || 0})
    this.setData({ imagePadding: Number(this.data.data.jsonData.imagePadding) || 0})
    console.log('=====MoFang-width-height-imagePadding====', this.data.width,this.data.height,this.data.imagePadding)
  },
  methods: {
    tolinkUrl: function (e) {
      let linkUrl = e.currentTarget.dataset.link
      app.globalData.linkEvent(linkUrl)
    }
  }
})