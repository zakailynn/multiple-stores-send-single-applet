const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {}
  },
  ready: function () {
      let that=this;
    // app.globalData.consoleFun("=====newsList组件-data=====",[that.data.data])
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.consoleFun("=====newsList组件-id=====",[event.currentTarget.dataset.id])
	  let linkUrl='news_detail.html?id=' + event.currentTarget.dataset.id;
	  app.globalData.linkEvent(linkUrl)
    }
  },
})